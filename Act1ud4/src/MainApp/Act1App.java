/**
 * 
 */
package MainApp;

/**
 * @author Elisabet Sabat�
 *
 */
public class Act1App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		double a=3, b=2;
		
		double suma = a+b;
		double resta = a-b;
		double divisio = a/b;
		double modulo = a%b;
		
		System.out.println("Els n�meros seleccionats s�n a: " +a +" i b: "+b);
		System.out.println("suma: "+suma);
		System.out.println("resta: "+resta);
		System.out.println("divisio: "+divisio);
		System.out.println("modulo: "+modulo);

	}

}
